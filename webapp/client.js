// Address of the server.
//const url = "ws://localhost:8080/ws";
const url = "wss://kovalev.best/ad/ws";

// DOM elements.
const roomSelectionContainer = document.getElementById('room-selection-container')
const roomInput = document.getElementById('room-input')
const connectButton = document.getElementById('connect-button')


const videoChatContainer = document.getElementById('video-chat-container')
const videoComponent = document.getElementById('video')
//const remoteVideoComponent = document.getElementById('remote-video')

var creatorId
var isRoomCreator = false

const mediaConstraints = {
  audio: true,
  video: { width: 1280, height: 720 },
}

// Free public STUN servers provided by Google.
const iceServers = {
  iceServers: [
    { urls: 'stun:stun.l.google.com:19302' },
    { urls: 'stun:stun1.l.google.com:19302' },
    { urls: 'stun:stun2.l.google.com:19302' },
    { urls: 'stun:stun3.l.google.com:19302' },
  ],
}

var peerConnections = {}

connectButton.addEventListener('click', () => {
  joinRoom(roomInput.value)
})



async function reactToMessage(event) {
  console.log("Received data from websocket: ", event.data);
  data = JSON.parse(event.data);
  
  switch (data.event) {
    case "room_created":
      console.log("Created room: ", data.data);
      await setLocalStream(mediaConstraints)
      isRoomCreator = true
      break;
    case "room_joined":
      // Request offer
      console.log("Joined room: ", data.data);

      creatorId = data.data.creator

      var request = {
        event: "request_offer",
        data: roomId,
      }
      socketSend(JSON.stringify(request));
      break;
    case "request_offer":
      console.log("Received request for offer from peer: ", data.data.client_id);
      if (!isRoomCreator) {
        console.log("Error: bogus request!");
        break;
      }

      var rtcPeerConnection = new RTCPeerConnection(iceServers)
      addLocalTracks(rtcPeerConnection)
      //rtcPeerConnection.ontrack = setRemoteStream
      rtcPeerConnection.onicecandidate = sendIceCandidateClosure(data.data.client_id)
      await createOffer(rtcPeerConnection, data.data.client_id)
      peerConnections[data.data.client_id] = rtcPeerConnection
      break;
    case "webrtc_offer":
      console.log("Received webrtc_offer from peer: ", data.data.client_id);
      if (isRoomCreator) {
        console.log("Error: bogus offer!");
        break;
      }

      rtcPeerConnection = new RTCPeerConnection(iceServers)
      peerConnections[creatorId] = rtcPeerConnection
      //addLocalTracks(rtcPeerConnection)
      rtcPeerConnection.ontrack = setRemoteStream
      rtcPeerConnection.onicecandidate = sendIceCandidateClosure(creatorId)
      rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(data.data.data.sdp))
      await createAnswer(rtcPeerConnection, creatorId)
      break;
    case "webrtc_answer":
      console.log("Received webrtc_answer from peer: ", data.data.sender_id);
      if (!isRoomCreator) {
        console.log("Error: bogus answer!");
        break;
      }

      var rtcPeerConnection = peerConnections[data.data.sender_id]
      rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(data.data.data.sdp))
      break;
    case "webrtc_ice_candidate":
      console.log("Received webrtc_ice_candidate from peer: ", data.data.sender_id);

      var rtcPeerConnection = peerConnections[data.data.sender_id]
      var candidate = new RTCIceCandidate({
        sdpMLineIndex: data.data.data.label,
        candidate: data.data.data.candidate,
      })
      rtcPeerConnection.addIceCandidate(candidate)
      break;
    case "refresh_count":
      var users = data.data.users;
      console.log("Received refresh_count qtty: ", users);
      setCount(users);
      break;
    case "room_deleted":
      console.log("Room deleted: ", data.data.room_id);
      handleRoomDeleted();
      break;
  } 
}

let socket = null;

function connect() {
  socket = new WebSocket(url);
  console.log("Attempting Connection to " + url + " ...");
  socket.onmessage = (event) => {
    reactToMessage(event);
  }

  socket.onclose = (event) => {
    // Retry connection
    setTimeout(function () {
      connect();
    }, 5000);
  }

  socket.onerror = function(err) {
    console.error('Socket encountered error: ', err.message, 'Closing socket');
    socket.close();
  };
}

function socketSend(data) {
  console.log("Sending data to websocket: ", data);
  socket.send(data);
}

connect();




function joinRoom(room) {
  if (room === '') {
    alert('Please type a room ID')
  } else {
    roomId = room
    socketSend(JSON.stringify({
      event: "join",
      data: roomId,
    }))
    showVideoConference()
  }
}

function showVideoConference() {
  roomSelectionContainer.style = 'display: none'
  videoChatContainer.style = 'display: block'
}

async function setLocalStream(mediaConstraints) {
  let stream
  try {
    stream = await navigator.mediaDevices.getUserMedia(mediaConstraints)
  } catch (error) {
    console.error('Could not get user media', error)
  }


  localStream = stream
  videoComponent.srcObject = stream
}

function setRemoteStream(event) {
  videoComponent.srcObject = event.streams[0]
  remoteStream = event.stream
}

async function createOffer(rtcPeerConnection, client_id) {
  let sessionDescription
  try {
    sessionDescription = await rtcPeerConnection.createOffer()
    rtcPeerConnection.setLocalDescription(sessionDescription)
  } catch (error) {
    console.error(error)
  }


  data = {
    event: 'webrtc_offer',
    data: {
      type: 'webrtc_offer',
      sdp: sessionDescription,
      client_id: client_id,
      room_id: roomId,
    }
  }

  socketSend(JSON.stringify(data)); 
}

async function createAnswer(rtcPeerConnection, client_id) {
  let sessionDescription
  try {
    sessionDescription = await rtcPeerConnection.createAnswer()
    rtcPeerConnection.setLocalDescription(sessionDescription)
  } catch (error) {
    console.error(error)
  }


  data = {
    event: 'webrtc_answer', data: {
      type: 'webrtc_answer',
      sdp: sessionDescription,
      room_id: roomId,
      client_id: client_id,
    }
  }
  socketSend(JSON.stringify(data)); 
}

function addLocalTracks(rtcPeerConnection) {
  localStream.getTracks().forEach((track) => {
    rtcPeerConnection.addTrack(track, localStream)
  })
}

function sendIceCandidateClosure(client_id) {
  return (event) => {
    if (event.candidate) {
      var data = {
        event: 'webrtc_ice_candidate', data:{
          room_id: roomId,
          label: event.candidate.sdpMLineIndex,
          candidate: event.candidate.candidate,
          client_id: client_id,
        }
      }

      socketSend(JSON.stringify(data));
    }
  }
}

function setCount(count) {
  document.getElementById("users-count-number").innerHTML = count;
}

function handleRoomDeleted() {
  alert("Room deleted!");
  window.location.reload();
}
