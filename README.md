# WebRTC video streaming/video chat application
This repository contains a basic video streaming application using WebRTC.
The application consists of a simple JavaScript web frontend and a Go backend
server, using websockets and events for communication.
## Functionality
The application allows a user to create a room and stream their video, and other
other users to join it and watch the stream. The application doesn't impose a
limit on the number of rooms that can be created, and users that can join
any of the rooms, though a room creator must consider the bandwidth since
every user joining will have a WebRTC connection with the creator.

The video streaming is unidirectional, since the application is very simple
and has been made to study the basics of WebRTC and websockets. Though I believe
it can easilly be extended to allow for a group video chat functionality.
## Application structure
The project contains two main parts: the Go backend and the JavaScript frontend.
The backend can be found in the `webrtc` directory. It is written in Go and
uses the `gorilla/mux` and `gorilla/websocket` packages for routing and websockets.

The frontend is located in the `webapp` directory. It is a simple static web page,
with a single `index.html` file, containing the HTML and CSS code, and a `client.js`
file with the JavaScript code. The frontend does not have any dependencies and uses
plain ES6 libraries.
## Running the application
To run the backend server, you first need to [install Go](https://golang.org/doc/install).
Then, you can run the following commands to start the server:
```bash
go run .
```
This should start the server on `*:8080`.

Since the frontend is a simple static web page, you can serve it using any
web server or directly open the `index.html` file with your preferred web
browser.
## Inspiration and acknowledgements
This application has beed inspired by the following article
[How to Implement a Video Conference with WebRTC and Node]<https://acidtango.com/thelemoncrunch/how-to-implement-a-video-conference-with-webrtc-and-node/>
by [Borja Nebbal Díaz](https://acidtango.com/thelemoncrunch/author/borja/). It is reusing a part of the frontend, though the plain ES6 websockets library
has been used instead of socket.io, and the backend has been completely rewritten in Go.
Check out the original article for a detailed explanation of the WebRTC protocol, application architecture and an implementation using Node.js and socket.io.
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.