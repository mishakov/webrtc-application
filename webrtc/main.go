package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/ws", wsHandler)

	mid := handlers.CombinedLoggingHandler(log.Writer(), r)

	http.Handle("/", mid)

	fmt.Println("Server started at :8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalln("There was an error starting the server:", err)
	}

}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     checkOrigin,
}

func checkOrigin(r *http.Request) bool {
	return true
}

type Room struct {
	// List of connections in the room
	Conns map[int64]*threadSafeWriter

	// Name of the room
	Name string

	// Creator of the room
	Creator *threadSafeWriter
}

type Message struct {
	Event string      `json:"event"`
	Data  interface{} `json:"data"`
}

type RoomCount struct {
	Users  int    `json:"users"`
	RoomId string `json:"room_id"`
}

type StartCall struct {
	RoomId string `json:"room_id"`
}

type JoinError struct {
	Message string `json:"message"`
	RoomId  string `json:"room_id"`
}

type RoomDeleted struct {
	RoomId string `json:"room_id"`
}

type RoomJoined struct {
	RoomId  string `json:"room_id"`
	Creator int64  `json:"creator"`
}

var rooms = make(map[string]*Room)
var roomsLock sync.Mutex

func wsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	c := initThreadSafeWriter(conn)

	// When this frame returns close the Websocket
	defer c.Close() //nolint

	for {
		_, p, err := c.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}

		log.Println("Message received:", string(p))

		var msg Message
		err = json.Unmarshal(p, &msg)
		if err != nil {
			log.Println(err)
			continue
		}

		switch msg.Event {
		case "join":
			joinRoom(c, msg.Data.(string))
		case "request_offer":
			requestOffer(c, msg.Data.(string))
		case "webrtc_offer":
			webrtcOffer(c, msg.Data)
		case "webrtc_answer":
			webrtcAnswer(c, msg.Data)
		case "webrtc_ice_candidate":
			webrtcIceCandidate(c, msg.Data)
		}

	}
}

type WebRTCOffer struct {
	Type     string `json:"type"`
	SDP      string `json:"sdp"`
	ClientID int64  `json:"client_id"`
	RoomId   string `json:"room_id"`
}

func webrtcOffer(c *threadSafeWriter, data interface{}) {
	roomsLock.Lock()
	defer roomsLock.Unlock()

	roomId := data.(map[string]interface{})["room_id"].(string)
	clientId := int64(data.(map[string]interface{})["client_id"].(float64))

	ptr, ok := rooms[roomId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_offer_error",
			Data: RequestOfferError{
				Message: "Room not found",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	client, ok := ptr.Conns[clientId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_offer_error",
			Data: RequestOfferError{
				Message: "You are not in the room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	// Send the data to the client
	err := client.WriteJSON(Message{
		Event: "webrtc_offer",
		Data:  WebRTCAnswer{SenderID: c.id, Data: data},
	})
	if err != nil {
		log.Println(err)
		return
	}
}

type WebRTCAnswer struct {
	SenderID int64       `json:"sender_id"`
	Data     interface{} `json:"data"`
}

func webrtcAnswer(c *threadSafeWriter, data interface{}) {
	if data.(map[string]interface{})["room_id"] == nil || data.(map[string]interface{})["client_id"] == nil {
		// Send error to the client
		err := Message{
			Event: "webrtc_answer_error",
			Data: RequestOfferError{
				Message: "Invalid data",
				RoomId:  "",
			},
		}

		c.WriteJSON(err)
		return
	}

	roomsLock.Lock()
	defer roomsLock.Unlock()

	roomId := data.(map[string]interface{})["room_id"].(string)
	clientId := int64(data.(map[string]interface{})["client_id"].(float64))

	ptr, ok := rooms[roomId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_answer_error",
			Data: RequestOfferError{
				Message: "Room not found",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	client, ok := ptr.Conns[clientId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_answer_error",
			Data: RequestOfferError{
				Message: "You are not in the room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	// Send the data to the client
	err := client.WriteJSON(Message{
		Event: "webrtc_answer",
		Data:  WebRTCAnswer{SenderID: c.id, Data: data},
	})
	if err != nil {
		log.Println(err)
		return
	}
}

func webrtcIceCandidate(c *threadSafeWriter, data interface{}) {
	if data.(map[string]interface{})["room_id"] == nil || data.(map[string]interface{})["client_id"] == nil {
		// Send error to the client
		err := Message{
			Event: "webrtc_ice_candidate_error",
			Data: RequestOfferError{
				Message: "Invalid data",
				RoomId:  "",
			},
		}

		c.WriteJSON(err)
		return
	}

	roomsLock.Lock()
	defer roomsLock.Unlock()

	roomId := data.(map[string]interface{})["room_id"].(string)
	clientId := int64(data.(map[string]interface{})["client_id"].(float64))

	ptr, ok := rooms[roomId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_ice_candidate_error",
			Data: RequestOfferError{
				Message: "Room not found",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	client, ok := ptr.Conns[clientId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "webrtc_ice_candidate_error",
			Data: RequestOfferError{
				Message: "You are not in the room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	// Send the data to the client
	err := client.WriteJSON(Message{
		Event: "webrtc_ice_candidate",
		Data:  WebRTCAnswer{SenderID: c.id, Data: data},
	})
	if err != nil {
		log.Println(err)
		return
	}
}

type RequestOfferError struct {
	Message string `json:"message"`
	RoomId  string `json:"room_id"`
}

type RequestOffer struct {
	ClientID int64 `json:"client_id"`
}

func requestOffer(c *threadSafeWriter, roomId string) {
	roomsLock.Lock()
	defer roomsLock.Unlock()

	ptr, ok := rooms[roomId]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "request_offer_error",
			Data: RequestOfferError{
				Message: "Room not found",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	_, ok = ptr.Conns[c.id]
	if !ok {
		// Send error to the client
		err := Message{
			Event: "request_offer_error",
			Data: RequestOfferError{
				Message: "You are not in the room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	if ptr.Creator == c {
		// Send error to the client
		err := Message{
			Event: "request_offer_error",
			Data: RequestOfferError{
				Message: "You are the creator of the room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
		return
	}

	// Send the data to the creator
	err := ptr.Creator.WriteJSON(Message{
		Event: "request_offer",
		Data: RequestOffer{
			ClientID: c.id,
		},
	})
	if err != nil {
		log.Println(err)
		return
	}
}

func joinRoom(c *threadSafeWriter, roomId string) {
	if c.getRoom() != nil {
		// Send error to the client
		err := Message{
			Event: "join_error",
			Data: JoinError{
				Message: "You are already in a room",
				RoomId:  roomId,
			},
		}

		c.WriteJSON(err)
	}

	roomsLock.Lock()
	defer roomsLock.Unlock()

	ptr, ok := rooms[roomId]

	if !ok {
		room := &Room{
			Conns: map[int64]*threadSafeWriter{
				c.id: c,
			},
			Name:    roomId,
			Creator: c,
		}

		msg := Message{
			Event: "room_created",
			Data:  roomId,
		}

		err := c.WriteJSON(msg)
		if err != nil {
			log.Println(err)
			return
		}

		usersData := RoomCount{
			Users:  len(room.Conns),
			RoomId: roomId,
		}

		// Send the data to the client
		err = c.WriteJSON(Message{
			Event: "refresh_count",
			Data:  usersData,
		})
		if err != nil {
			log.Println(err)
			return
		}

		rooms[roomId] = room
		c.ActiveRoom = room
		return
	}

	ptr.Conns[c.id] = c
	c.ActiveRoom = ptr

	// Send the room_joined event to the client
	err := c.WriteJSON(Message{
		Event: "room_joined",
		Data:  RoomJoined{RoomId: roomId, Creator: ptr.Creator.id},
	})
	if err != nil {
		log.Println(err)
		return
	}

	// Update the count for all the users
	for _, conn := range ptr.Conns {
		go func(conn *threadSafeWriter) {
			usersData := RoomCount{
				Users:  len(ptr.Conns),
				RoomId: roomId,
			}

			// Send the data to the client
			err := conn.WriteJSON(Message{
				Event: "refresh_count",
				Data:  usersData,
			})
			if err != nil {
				log.Println(err)
				return
			}
		}(conn)
	}
}

func broadcastEvent(roomId string, author *threadSafeWriter, msg interface{}) {
	roomsLock.Lock()
	defer roomsLock.Unlock()

	ptr, ok := rooms[roomId]
	if !ok {
		return
	}

	syncGroup := sync.WaitGroup{}
	toDelete := []*threadSafeWriter{}

	for _, conn := range ptr.Conns {
		if conn == author {
			continue
		}

		syncGroup.Add(1)
		go func(conn *threadSafeWriter) {
			defer syncGroup.Done()
			// Send the data to the client
			err := conn.WriteJSON(Message{
				Event: "new_message",
				Data:  msg,
			})
			if err != nil {
				log.Println(err)
				toDelete = append(toDelete, conn)
				return
			}
		}(conn)
	}

	syncGroup.Wait()
	for _, conn := range toDelete {
		delete(ptr.Conns, conn.id)
	}

	if len(toDelete) != 0 {
		// Notify all the users about the new count
		for _, conn := range ptr.Conns {
			go func(conn *threadSafeWriter) {
				usersData := RoomCount{
					Users:  len(ptr.Conns),
					RoomId: roomId,
				}

				// Send the data to the client
				err := conn.WriteJSON(Message{
					Event: "refresh_count",
					Data:  usersData,
				})
				if err != nil {
					log.Println(err)
					return
				}
			}(conn)
		}
	} else if len(ptr.Conns) == 0 {
		delete(rooms, roomId)
	}
}

type threadSafeWriter struct {
	*websocket.Conn
	sync.Mutex
	id         int64
	ActiveRoom *Room
}

func (t *threadSafeWriter) WriteJSON(v interface{}) error {
	t.Lock()
	defer t.Unlock()

	return t.Conn.WriteJSON(v)
}

func (t *threadSafeWriter) Close() error {
	t.Lock()
	err := t.Conn.Close()
	activeRoom := t.ActiveRoom
	t.Unlock()

	if activeRoom != nil {
		roomsLock.Lock()
		defer roomsLock.Unlock()
		delete(activeRoom.Conns, t.id)

		if activeRoom.Creator == t {
			for _, conn := range activeRoom.Conns {
				go func(conn *threadSafeWriter) {
					// Send the data to the client
					err := conn.WriteJSON(Message{
						Event: "room_deleted",
						Data:  RoomDeleted{RoomId: activeRoom.Name},
					})
					if err != nil {
						log.Println(err)
						return
					}
				}(conn)
			}
			log.Println("Room deleted:", activeRoom.Name)

			delete(rooms, activeRoom.Name)
		} else if len(activeRoom.Conns) == 0 {
			delete(rooms, activeRoom.Name)
			log.Println("Room deleted:", activeRoom.Name)
		} else {
			for _, conn := range activeRoom.Conns {
				go func(conn *threadSafeWriter) {
					usersData := RoomCount{
						Users:  len(activeRoom.Conns),
						RoomId: activeRoom.Name,
					}

					// Send the data to the client
					err := conn.WriteJSON(Message{
						Event: "refresh_count",
						Data:  usersData,
					})
					if err != nil {
						log.Println(err)
						return
					}
				}(conn)
			}
		}
	}

	log.Print("Closed connection for id:", t.id)

	return err
}

func (t *threadSafeWriter) setRoom(room *Room) {
	t.Lock()
	defer t.Unlock()

	t.ActiveRoom = room
}

func (t *threadSafeWriter) getRoom() *Room {
	t.Lock()
	defer t.Unlock()

	return t.ActiveRoom
}

var lastId int64 = 0
var lastIdLock sync.Mutex

func initThreadSafeWriter(conn *websocket.Conn) *threadSafeWriter {
	lastIdLock.Lock()
	defer lastIdLock.Unlock()
	lastId++
	return &threadSafeWriter{Conn: conn, id: lastId}
}
